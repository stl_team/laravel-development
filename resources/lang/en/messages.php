<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Access is denied due to invalid credentials.',
    'unauthenticated' => 'Your session has expired. Please log in again.',
    //'no_such_user_exist' => 'No such user exist.',

    'success' => 'Success',
    'error' => 'Error',

    'no_todos' => 'No to-dos found.',
    'todo_create_success' => 'To-do added successfully.',
    'no_such_todo' => 'No such to-do exist.',
    'todo_update_success' => 'To-do updated successfully.',
    'todo_delete_success' => 'To-do deleted successfully.',
];
