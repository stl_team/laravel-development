<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'V1\LoginController@login')->name('login');
Route::post('register', 'V1\LoginController@register')->name('register');

Route::group(['middleware' => 'auth:api'], function () {
	Route::resource('todos', 'V1\TodoController');
    Route::get('todos/markComplete/{id}', 'V1\TodoController@markComplete');
    Route::post('logout', 'V1\LoginController@logout')->name('logout');
});
