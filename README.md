# laravel-development

# Installation

* Install dependencies install composer based on your OS(Windows,ubuntu), Here is the reference link.
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04
* Clone the repository.
	git clone https://stl_rahulmehta@bitbucket.org/stl_team/laravel-development.git
* In this directory (where this file is located), open a Terminal.
* Update composer will update all the package dependencies
	composer update
* To serve your application, you may use the serve Artisan command
 	php artisan serve