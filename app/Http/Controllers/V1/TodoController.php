<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Todo;
use Auth;
use Validator;
use App\Helpers\Comman;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $get_todos = Todo::where('user_id', $user->id)->get();
        if (count($get_todos)) {
            $row = array('status' => '200','messages' => trans('messages.success'),'data'=> $get_todos);
            return response()->json($row, 200);
        } else {
            $row = array('status' => '200','messages' => trans('messages.no_todos'),'data'=> array());
            return response()->json($row, 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = array('status' => '200','messages' => trans('messages.error'),'data'=> array());

        $validator = $this->addTodoValidation($request);
		$err_str = '';
        if ($validator->fails()) {
            $err_str = Comman::getErrorStr($validator->errors());
            $row = array('status' => '400', 'messages' => $err_str, 'data' => array());
            return response()->json($row, 400);
        }

        $user_id = Auth::id();

        $todo = new Todo;
        $todo->name = $request->input('name');
        $todo->user_id = $user_id;
        $todo->save();

        $todo = Todo::find($todo->id);

        $row = array('status' => '200','messages' => trans('messages.todo_create_success'),'data'=> $todo);
        return response()->json($row, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todo = Todo::find($id);
        if ($todo != null) {
            $todo = Todo::find($todo->id);

            $row = array('status' => '200','messages' => trans('messages.success'),'data'=> $todo);
            return response()->json($row, 200);
        } else {
            $row = array('status' => '400','messages' => trans('messages.no_such_todo'),'data'=> array());
            return response()->json($row, 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row = array('status' => '200','messages' => trans('messages.error'),'data'=> array());

        $validator = $this->addTodoValidation($request);
        $err_str = '';
        if ($validator->fails()) {
            $err_str = Comman::getErrorStr($validator->errors());
            $row = array('status' => '400', 'messages' => $err_str, 'data' => array());
            return response()->json($row, 400);
        }

        $todo = Todo::find($id);
        if ($todo != null) {
            $todo->name = $request->input('name');
            $todo->save();

            $todo = Todo::find($todo->id);

            $row = array('status' => '200','messages' => trans('messages.todo_update_success'),'data'=> $todo);
            return response()->json($row, 200);
        } else {
            $row = array('status' => '400','messages' => trans('messages.no_such_todo'),'data'=> array());
            return response()->json($row, 400);
        }
    }
	
	public function markComplete($id)
	{
		$todo = Todo::find($id);
        if ($todo != null) {
            $todo->status = 1;
            $todo->save();

            $todo = Todo::find($todo->id);

            $row = array('status' => '200','messages' => trans('messages.success'),'data'=> $todo);
            return response()->json($row, 200);
        } else {
            $row = array('status' => '400','messages' => trans('messages.no_such_todo'),'data'=> array());
            return response()->json($row, 400);
        }
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        if ($todo != null) {
            $todo->delete();
            $row = array('status' => '200','messages' => trans('messages.todo_delete_success'),'data'=> array());
            return response()->json($row, 200);
        } else {
            $row = array('status' => '400','messages' => trans('messages.no_such_todo'),'data'=> array());
            return response()->json($row, 400);
        }
    }

    private function addTodoValidation(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required',
        ]);
    }
}
