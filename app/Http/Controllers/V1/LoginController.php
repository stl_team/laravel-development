<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Hash;
use Auth;
use App\Helpers\Comman;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = $this->loginValidator($request);

        $err_str = '';
        if ($validator->fails()) {
            $err_str = Comman::getErrorStr($validator->errors());
            $row = array('status' => '400', 'messages' => $err_str, 'data' => array());
            return response()->json($row, 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            // @todo Make this string a class constant
            $scopes[] = '';
            $this->revokeTokens($user);
            return response()->json(['token' => $user->createToken('MyApp', $scopes)->accessToken, 'user' => $user]);
        } else {
            $row = array('status' => '401', 'messages' => trans('messages.failed'), 'data' => array());
            return response()->json($row, 401);
        }
    }

    public function register(Request $request)
    {
        $validator = $this->RegisterValidator($request);

        $err_str = '';
        if ($validator->fails()) {
            $err_str = Comman::getErrorStr($validator->errors());
            $row = array('status' => '400', 'messages' => $err_str, 'data' => array());
            return response()->json($row, 400);
        }

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'phone' => $request->input('phone'),
        ]);

        return response()->json(['token' => $user->createToken('MyApp')->accessToken, 'user' => $user]);
    }

    public function logout()
    {
        Auth::user()->token()->revoke();
        return response()->json();
    }
    
    private function loginValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
        ]);
    }

    private function RegisterValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required',
        ]);
    }

    private function revokeTokens(User $user)
    {
        // Remove any existing tokens
        foreach ($user->tokens as $token) {
            $token->revoke();
        }
    }
}
