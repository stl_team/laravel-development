<?php

namespace App\Helpers;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class Comman
{
	/**
	 * Get Validation Error string
	 *
	 * @param $errors
	 * @return mixed|string
	 */
    public static function getErrorStr($errors)
    {
        $err_str = '';
        if (count($errors)) {
            $error_array = $errors->toArray();
            $first_elem = reset($error_array);
            $err_str = $first_elem[0];
        }
        return $err_str;
    }
}
